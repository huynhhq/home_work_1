import '../common/constants.dart';

class CommonUI {
  const CommonUI(
      {this.title,
      this.type,
      this.paddingLeft,
      this.paddingTop,
      this.paddingRight,
      this.paddingBottom,
      this.src,
      this.index});

  final String title;
  final String type;
  final double paddingLeft;
  final double paddingTop;
  final double paddingRight;
  final double paddingBottom;
  final String src;
  final int index;
}

final List<CommonUI> allCommonModels = <CommonUI>[
  const CommonUI(
      title: kTitleTextFieldTypeC,
      type: kTextFieldType,
      paddingLeft: 0,
      paddingTop: 0,
      paddingRight: 0,
      paddingBottom: 0,
      index: 0),
  const CommonUI(
      title: kTitleLableTypeC,
      type: kTextType,
      paddingLeft: 0,
      paddingTop: 20,
      paddingRight: 0,
      paddingBottom: 20,
      index: 1),
  const CommonUI(
      title: "",
      type: kImageType,
      paddingLeft: 0,
      paddingTop: 0,
      paddingRight: 0,
      paddingBottom: 10,
      src: kTwoArrows,
      index: 2),
  const CommonUI(
      title: kTitleLableTypeF,
      type: kTextType,
      paddingLeft: 0,
      paddingTop: 20,
      paddingRight: 0,
      paddingBottom: 20,
      index: 3),
  const CommonUI(
      title: kTitleTextFieldTypeF,
      type: kTextFieldType,
      paddingLeft: 0,
      paddingTop: 0,
      paddingRight: 0,
      paddingBottom: 0,
      index: 4),
];
