//App
const String kNameApplication = "Temperature App";
const String kTitleHomePage = "Celsius & Fahrenheit";
const String kTitleLableTypeC = "Celsius";
const String kTitleLableTypeF = "Fahrenheit";
const String kTitleTextFieldTypeC = "Enter Celsius";
const String kTitleTextFieldTypeF = "Enter Fahrenheit";

//Type
const String kTextType = "Text";
const String kTextFieldType = "TextField";
const String kImageType = "Image";

//Messages
const String kInstruction =
    "* Note: Please enter celsius to change from celsius to fahrenheit and vice versa.";

//Assets
const String kTwoArrows = "assets/images/two_arrows.png";
