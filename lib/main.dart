import 'package:flutter/material.dart';
import 'common/constants.dart';
import './models/common_ui.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kNameApplication,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: kTitleHomePage),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _cForCelsius = TextEditingController();
  final _cForFahrenheit = TextEditingController();
  final _focusNodeC = new FocusNode();
  final _focusNodeF = new FocusNode();
  final doubleRegex = RegExp(r'\s+(\d+\.\d+)\s+', multiLine: true);

  @override
  void initState() {
    _cForCelsius.addListener(_convertCelsiusToFahrenheit);
    _cForFahrenheit.addListener(_convertFahrenheitToCelsius);
    _focusNodeC.addListener(_convertCelsiusFocus);
    _focusNodeF.addListener(_convertFahrenheitFocus);
    super.initState();
  }

  @override
  void dispose() {
    _cForCelsius.dispose();
    _cForFahrenheit.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          alignment: AlignmentDirectional.center,
          margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          decoration: BoxDecoration(
              border: Border.all(
                  width: 2, color: Colors.grey, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: ListView(
            shrinkWrap: true,
            children: _generateTitleAndTextFieldItems(context),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Text(
            kInstruction,
            style: TextStyle(color: Colors.redAccent, fontSize: 17),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  Iterable<Widget> _generateTitleAndTextFieldItems(BuildContext context) {
    final List<Widget> result = <Widget>[];
    allCommonModels.sort((a, b) => a.index.compareTo(b.index));
    for (var item in allCommonModels) {
      switch (item.type) {
        case kTextType:
          result.add(
            Container(
                padding: EdgeInsets.fromLTRB(item.paddingLeft, item.paddingTop,
                    item.paddingRight, item.paddingBottom),
                child: Text(item.title, textAlign: TextAlign.center)),
          );
          break;

        case kTextFieldType:
          result.add(
            Container(
              padding: EdgeInsets.fromLTRB(item.paddingLeft, item.paddingTop,
                  item.paddingRight, item.paddingBottom),
              child: TextField(
                controller: item.title == kTitleTextFieldTypeC
                    ? _cForCelsius
                    : _cForFahrenheit,
                focusNode: item.title == kTitleTextFieldTypeC
                    ? _focusNodeC
                    : _focusNodeF,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: item.title,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(),
                  ),
                ),
              ),
            ),
          );
          break;

        case kImageType:
          result.add(
            Container(
              constraints: BoxConstraints.expand(height: 40),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    item.src,
                    fit: BoxFit.cover,
                  )
                ],
              ),
            ),
          );
          break;
        default:
      }
    }
    return result;
  }

  void _convertCelsiusToFahrenheit() {
    var convertedValue = double.parse(_cForCelsius.text) * 1.8 + 32;
    _cForFahrenheit.text = "${convertedValue}°F";
  }

  void _convertFahrenheitToCelsius() {
    var convertedValue = (double.parse(_cForFahrenheit.text) - 32) / 1.8;
    _cForCelsius.text = "${convertedValue}°C";
  }

  void _convertCelsiusFocus() {
    if (_focusNodeC.hasFocus) {
      var realValue = _cForCelsius.text.replaceAll(r'°C', '');
      _cForCelsius.text = realValue;
    } else {
      _cForCelsius.text = "${_cForCelsius.text}°C";
    }
  }

  void _convertFahrenheitFocus() {
    if (_focusNodeF.hasFocus) {
      var realValue = _cForFahrenheit.text.replaceAll(r'°F', '');
      _cForFahrenheit.text = realValue;
    } else {
      _cForFahrenheit.text = "${_cForFahrenheit.text}°F";
    }
  }
}
